package com.evaluator_project_1640612.unittest;

import com.evaluator_project_1640612.business.Evaluator;
import com.evaluator_project_1640612.exceptions.InvalidStringInInputQueue;
import com.evaluator_project_1640612.exceptions.NonBinaryExpression;
import com.evaluator_project_1640612.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import javax.script.ScriptException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test class for expression with invalid string
 * 
 * @author Hadil Elhashani
 */
@RunWith(Parameterized.class)
public class InvalidStringInInputTest {
    @Parameterized.Parameters(name = "{index} evaluator[{0}]]")
    public static Collection<Object[]> data() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("2" ,"+","p"))},
            {new ArrayDeque<>()},
            {new ArrayDeque<>(Arrays.asList("2" ,"4","5"))},
            {new ArrayDeque<>(Arrays.asList("-" ,"/","/"))},
            {new ArrayDeque<>(Arrays.asList("x","-","4"))},
            {new ArrayDeque<>(Arrays.asList("2", "+", "-", "*", "4"))}
        });
    }
    
    private Queue<String> input;
    
    /**
     * Initializes the queue 
     * 
     * @param input 
     */
    public InvalidStringInInputTest(Queue<String> input) {
        this.input = input;
    }
    
    /**
     * Test method where it expects the initialization to throw a InvalidStringInInputQueue exception
     * 
     * @throws InvalidStringInInputQueue
     * @throws NonMatchingParenthesis
     * @throws NonBinaryExpression
     * @throws ScriptException 
     */
    @Test(expected = InvalidStringInInputQueue.class)
    public void testReslut() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        new Evaluator(input);
    }

}
