package com.evaluator_project_1640612.unittest;

import com.evaluator_project_1640612.business.Evaluator;
import com.evaluator_project_1640612.exceptions.InvalidStringInInputQueue;
import com.evaluator_project_1640612.exceptions.NonBinaryExpression;
import com.evaluator_project_1640612.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import javax.script.ScriptException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test class for non binary expression 
 * 
 * @author Hadil Elhashani
 */
@RunWith(Parameterized.class)
public class NoBinaryExpressionTest {
    @Parameterized.Parameters(name = "{index} evaluator[{0}]]")
    public static Collection<Object[]> data() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("2" ,"+"))},
            {new ArrayDeque<>(Arrays.asList("-","4"))},
            {new ArrayDeque<>(Arrays.asList("/","1"))},
            {new ArrayDeque<>(Arrays.asList("9","*"))},
            {new ArrayDeque<>(Arrays.asList("2", "+", "*", "4"))}
        });
    }
    
    private Queue<String> input;
    
    /**
     * Initializes the queue 
     * 
     * @param input 
     */
    public NoBinaryExpressionTest(Queue<String> input) {
        this.input = input;
    }
    
    /**
     * Test method where it expects the initialization to throw a NonBinaryExpression exception
     * 
     * @throws InvalidStringInInputQueue
     * @throws NonMatchingParenthesis
     * @throws NonBinaryExpression
     * @throws ScriptException 
     */
    @Test(expected = NonBinaryExpression.class)
    public void testReslut() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        new Evaluator(input);
    }
}
