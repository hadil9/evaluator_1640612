package com.evaluator_project_1640612.unittest;

import com.evaluator_project_1640612.business.Evaluator;
import com.evaluator_project_1640612.exceptions.InvalidStringInInputQueue;
import com.evaluator_project_1640612.exceptions.NonBinaryExpression;
import com.evaluator_project_1640612.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import javax.script.ScriptException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test class for converting infix expressions to postfix
 * 
 * @author Hadil Elhashani
 */
@RunWith(Parameterized.class)
public class InfixToPostfixTest {
    
    
    @Parameterized.Parameters(name = "{index} evaluator[{0}]={1}]")
    public static Collection<Object[]> data() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("2","+","2")), 4},
            {new ArrayDeque<>(Arrays.asList("(","1","*","2",")","-", "4","/","2")), 0},
            {new ArrayDeque<>(Arrays.asList("(","3","*","2",")","-","(", "8","*","6",")")),-42},
            {new ArrayDeque<>(Arrays.asList("2", "(", "3", "+", "2", ")")), 10},
            {new ArrayDeque<>(Arrays.asList("2", "*", "2", "-", "(","5","*","3",")")), -11},
            {new ArrayDeque<>(Arrays.asList("2", "+", "3", "*", "4")), 14},
            {new ArrayDeque<>(Arrays.asList("(","6","+","2",")","(", "5","*","5",")")),200},
            {new ArrayDeque<>(Arrays.asList("9", "*", "2", "-", "(","2","+","3",")")), 13},
            {new ArrayDeque<>(Arrays.asList("9", "*", "2", "-", "(","7","+","3",")")), 8},
            {new ArrayDeque<>(Arrays.asList("3","-","2","*","2")), -1},
            {new ArrayDeque<>(Arrays.asList("4","-","(","8","*","2",")" )), -12},
            {new ArrayDeque<>(Arrays.asList("(","6","/","2",")","(", "6","*","9",")")),162},
            {new ArrayDeque<>(Arrays.asList("7", "*", "2", "-", "(","6","+","2",")")), 6},
            {new ArrayDeque<>(Arrays.asList("5","+","2","*","5")), 15},
            {new ArrayDeque<>(Arrays.asList("4","*","2","/","2")), 4},
            {new ArrayDeque<>(Arrays.asList("6","(","7","+","4",")","-","(", "8","/","2","-","2",")","9")),48},
            {new ArrayDeque<>(Arrays.asList("3","(","5","+","4",")","-","(", "8","*","2","+","5",")","9")),-162},
            {new ArrayDeque<>(Arrays.asList("8","-","6","/","3")), 6},
            {new ArrayDeque<>(Arrays.asList("(","8","*","2",")","+", "4","/","2")), 18},
            {new ArrayDeque<>(Arrays.asList("(","8","*","2",")","(", "4","/","2",")")),32},
            {new ArrayDeque<>(Arrays.asList("1","-","6","/","3")), -1},
            {new ArrayDeque<>(Arrays.asList("4","+","4","(", "3","*","2",")","+","4","*","1")),32},
            {new ArrayDeque<>(Arrays.asList("(","6","+","2",")","(", "5","*","3",")")),120},
            {new ArrayDeque<>(Arrays.asList("7","+","(","5","+","3",")","(", "6","-","2",")")),39},
            {new ArrayDeque<>(Arrays.asList("5","(","3","+", "2","-","4",")")),5},
            {new ArrayDeque<>(Arrays.asList("(","6","/","3",")","+", "8","(","8","/","4","-","1",")")),10},
            {new ArrayDeque<>(Arrays.asList("(","8","+","7","*","3",")","5")),145},
            {new ArrayDeque<>(Arrays.asList("7","(","2","*","9",")","+","(", "8","-","4",")")),130},
            {new ArrayDeque<>(Arrays.asList("(","8","/","4",")","(", "9","-","2",")")),14},
            {new ArrayDeque<>(Arrays.asList("5","(","3","*","8",")","(", "5","-","2",")")),360},
            {new ArrayDeque<>(Arrays.asList("7","(","8","-","5",")","-","(", "2","*","8","/","2",")")),13},
            {new ArrayDeque<>(Arrays.asList("3","+","2","*","7")), 17},
            {new ArrayDeque<>(Arrays.asList("4","*","2","*","2","*","6")), 96},
            {new ArrayDeque<>(Arrays.asList("8","(","6","/","3",")")), 16},
            {new ArrayDeque<>(Arrays.asList("9","(","5","+","4",")","+","(", "8","/","2","-","1",")","7")),102}
        });
    }

    private Queue<String> input;
    private double expected;

    /**
     * Initializes the queue and expected value
     * 
     * @param input
     * @param expected 
     */
    public InfixToPostfixTest(Queue<String> input, double expected) {
        this.input = input;
        this.expected = expected;
    }
    
    /**
     * Test method where it checks that the solution of the postfix expression 
     * returns the right result
     * 
     * @throws InvalidStringInInputQueue
     * @throws NonMatchingParenthesis
     * @throws NonBinaryExpression
     * @throws ScriptException 
     */
    @Test
    public void testReslut() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        Evaluator e = new Evaluator(input);
        assertEquals(expected, e.evaluatePostfixExpression(),0.001);
    }
}
