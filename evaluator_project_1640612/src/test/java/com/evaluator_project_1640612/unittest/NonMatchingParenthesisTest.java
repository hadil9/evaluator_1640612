package com.evaluator_project_1640612.unittest;

import com.evaluator_project_1640612.business.Evaluator;
import com.evaluator_project_1640612.exceptions.InvalidStringInInputQueue;
import com.evaluator_project_1640612.exceptions.NonBinaryExpression;
import com.evaluator_project_1640612.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;
import javax.script.ScriptException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *  Test class for expression with non matching parenthesis  
 * 
 * @author Hadil Elhashani
 */
@RunWith(Parameterized.class)
public class NonMatchingParenthesisTest {
    @Parameterized.Parameters(name = "{index} evaluator[{0}]={1}]")
    public static Collection<Object[]> data() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        return Arrays.asList(new Object[][]{
            {new ArrayDeque<>(Arrays.asList("(","2" ,"+","3",")",")"))},
            {new ArrayDeque<>(Arrays.asList("(","(","4" ,"-","9",")"))},
            {new ArrayDeque<>(Arrays.asList("(","(","2", "+", "*", "4",")"))},
            {new ArrayDeque<>(Arrays.asList("(","(","2", "+","6",")","3", ")","+", "4",")"))},
            {new ArrayDeque<>(Arrays.asList("(","(","6", "/","6",")","-","3","+", "4",")" , ")"))}
        });
    }
    
    private Queue<String> input;
    
    /**
     * Initializes the queue 
     * 
     * @param input 
     */
    public NonMatchingParenthesisTest(Queue<String> input) {
        this.input = input;
    }
    
    /**
     * Test method where it expects the initialization to throw a NonMatchingParenthesis exception
     * 
     * @throws InvalidStringInInputQueue
     * @throws NonMatchingParenthesis
     * @throws NonBinaryExpression
     * @throws ScriptException 
     */
    @Test(expected = NonMatchingParenthesis.class)
    public void testReslut() throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        new Evaluator(input);
    }
}