package com.evaluator_project_1640612.exceptions;
/**
 * Custom exception to check if the input to the 
 * evaluator constructor does not have equal number
 * of parenthesis
 * 
 * @author Hadil Elhashani
 */
public class NonMatchingParenthesis extends Exception{

    private static final long serialVersionUID = 1L;

    public NonMatchingParenthesis(String msg) {
        super(msg);
    }
    
}