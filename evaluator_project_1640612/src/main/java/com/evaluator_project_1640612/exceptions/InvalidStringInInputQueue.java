package com.evaluator_project_1640612.exceptions;

/**
 * Custom exception to check if the input to the 
 * evaluator constructor is invalid
 * 
 * @author hadil elhashani
 */
public class InvalidStringInInputQueue extends Exception{

    private static final long serialVersionUID = 1L;

    public InvalidStringInInputQueue(String msg) {
        super(msg);
    }
    
}
