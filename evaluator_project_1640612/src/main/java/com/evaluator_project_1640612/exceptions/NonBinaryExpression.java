package com.evaluator_project_1640612.exceptions;

/**
 * Custom exception to check if the input to the 
 * evaluator constructor is non binary
 * 
 * @author hadil elhashani
 */
public class NonBinaryExpression extends Exception{

    private static final long serialVersionUID = 1L;

    public NonBinaryExpression(String msg) {
        super(msg);
    }
    
}