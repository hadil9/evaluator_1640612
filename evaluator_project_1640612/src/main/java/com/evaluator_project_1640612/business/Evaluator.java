package com.evaluator_project_1640612.business;

import com.evaluator_project_1640612.exceptions.InvalidStringInInputQueue;
import com.evaluator_project_1640612.exceptions.NonBinaryExpression;
import com.evaluator_project_1640612.exceptions.NonMatchingParenthesis;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.Queue;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Evaluator class evaluates infix to postfix expression using 
 * Java data structure 
 * 
 * @author Hadil Elhashani
 */
public class Evaluator {
    private final static Logger LOG = LoggerFactory.getLogger(Evaluator.class);
    private Deque<String> operators;
    private Queue<String> operands;
    private Queue<String> result;
    private Queue<String> initial;
    
    /**
     *
     * @param initial
     * 
     * @throws com.evaluator_project_1640612.exceptions.InvalidStringInInputQueue
     * @throws com.evaluator_project_1640612.exceptions.NonMatchingParenthesis
     * @throws com.evaluator_project_1640612.exceptions.NonBinaryExpression
     * @throws javax.script.ScriptException
     */
    public Evaluator(Queue<String> initial) throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression, ScriptException {
        validateInput(initial);
        this.operands = new ArrayDeque<>();
        this.operators = new ArrayDeque<>();
        this.result = new ArrayDeque<>();
        fetchTheExpression();
    }
    /**
     * 
     * @return 
     */
    @Override
    public String toString(){
        return this.initial.toString();
    }
    
    /**
     * Checks if the char is an operator 
     * 
     * @param c
     * @return 
     */
   private boolean isOperator(String c){
       return (c.equals("-") || c.equals("+") || c.equals("*") || c.equals("/"));
   }
   
   /**
    * Checks which operator has higher precedence than the other one
    * 
    * @param first
    * @param second
    * @return 
    */
   private boolean isEqualOrHigherPrecedence(String first, String second){
       if ((first.equals("*") || first.equals("/")) && (second.equals("*") || second.equals("/"))){
           return true;
       }
       else if ((first.equals("+") || first.equals("-")) && ( second.equals("+") || second.equals("-"))){
           return true;
       }
       return ((first.equals("*") || first.equals("/")) && ( second.equals("-")|| second.equals("+")));
   }
   
   /**
     * Checks if the char is an operand 
     * 
     * @param c
     * @return 
     */
   private boolean isOperand(String c){
        return c.matches("[0-9]+");
   }
   
   /**
    * Checks is char is a parenthesis
    * 
    * @param c
    * @return 
    */
   private boolean isParenthesis(String c){
       return (c.equals("(") || c.equals(")"));
   }
   
   /**
    * Checks if the expression contains anything other than 
    * operators or operands 
    * 
    * @param input
    * @return 
    */
   private boolean isValidExpression(Queue<String> input){
       Iterator<String> value = input.iterator();
       
       while (value.hasNext()) { 
            String c = value.next();
            if(!isOperand(c) && !isOperator(c) && !isParenthesis(c)){
                return false;
            } 
        }
       return true;
   }
   
   /**
    * Checks if the expression contains anything only operands 
    * or only operators
    * 
    * @param input
    * @return 
    */
   private boolean containsOperandsAndOperators(Queue<String> input){
       Iterator<String> value = input.iterator();
       int countOperators = 0; int countOperands = 0;
       while (value.hasNext()) { 
            String c = value.next();
            if(isOperand(c)){
                countOperands++;
            }
            else if(isOperator(c)){
                countOperators++;
            }
        }
       LOG.debug("count of operands = " + countOperands);
       LOG.debug("count of operators = " + countOperators);
       return (countOperands != input.size() && countOperators != input.size());
   }
   
   /**
    * Checks the count of operators and operands
    * 
    * @param input
    * @return 
    */
   private boolean isValidNumberOfOperandOperators(Queue<String> input){
       
       if(input.isEmpty()){
           return false;
       }
       int countOperands = 0;
       int countOperators = 0; 
       
       Iterator<String> value = input.iterator();
       while (value.hasNext()) { 
            String c = value.next();
            if(isOperand(c)){
                countOperands++;
            }
            else if(isOperator(c)){
                countOperators++;
            }
        }
       return countOperands >= countOperators;
   }
   
   
   /**
    * Validates the input queue 
    * 
    * @param initial
    */
   private void validateInput(Queue<String> initial) throws InvalidStringInInputQueue, NonMatchingParenthesis, NonBinaryExpression{
       LOG.debug("validateInput");
       if(initial.isEmpty() || !isValidExpression(initial) 
               || !containsOperandsAndOperators(initial) 
               || !isValidNumberOfOperandOperators(initial)){
           throw new InvalidStringInInputQueue("Input String is empty or invalid");
       }
       else if(hadMatchingParenthesis(initial)){
           throw new NonMatchingParenthesis("Input String does not have equal number of openning and closing parenthesis");
       }
       else if(isNonBinaryExpression(initial)){
           throw new NonBinaryExpression("Input String is not a binary exprssion");
       }
       evaluateTheInitailExpression(initial);
   }
   
   /**
    * Checks if the queue has equal number of opening and 
    * closing parenthesis
    * 
    * @param input
    * @return 
    */
   private boolean hadMatchingParenthesis(Queue<String> input){
       int countOpenningParenthesis = Collections.frequency(input, "(");
       int countClosingParenthesis = Collections.frequency(input, ")");  
       
       LOG.debug("count of OpenningParenthesis = " + countOpenningParenthesis);
       LOG.debug("count of ClosingParenthesis = " + countClosingParenthesis);
       
       return countOpenningParenthesis != countClosingParenthesis;
   }
   
   /**
    * Checks if the queue is a binary expression 
    * 
    * @param input
    * @return 
    */
   private boolean isNonBinaryExpression(Queue<String> input){ 
       LOG.info("isNonBinaryExpression");
       String previous = null; 
       Iterator<String> value = input.iterator();
       
       if(input.size() == 2){
           return true;
       }
       
       if(value.hasNext()){
           previous = value.next();
       }
       while (value.hasNext()) { 
           String current = value.next();
           
           if(isOperator(previous) && isOperator(current)){
               return true;
           }
           previous = current;
       }
       return false;
   }
      
   /**
    * Evaluates the input queue and checks if there is and opening
    * or closing parenthesis
    * 
    * @param input
    */
   private void evaluateTheInitailExpression(Queue<String> input){
       Queue<String> evaluatedInitial = new ArrayDeque<>();
       String previous = null; 
       Iterator<String> value = input.iterator();
       
       if(value.hasNext()){
           previous = value.next();
       }
       while (value.hasNext()) { 
           String current = value.next();
           String c = current;
           evaluatedInitial.add(previous);
           
           if(c.equals("(") && previous != null){
               if(previous.charAt(0) == ')' || isOperand(previous)){
                   evaluatedInitial.add("*");
               }
           }
           else if(isOperand(c) && previous != null){
               if(previous.charAt(0) == ')'){
                   evaluatedInitial.add("*");
               }
           }
           previous = current;
       }
       evaluatedInitial.add(previous);
       this.initial = evaluatedInitial;
       LOG.info("result after evaluating = " + evaluatedInitial.toString());
   }
   
   /**
    * 
    * Fetches the expression and put the operators and operands 
    * in their specified stack and queue
    * 
    * @throws ScriptException
    */
   private void fetchTheExpression() throws ScriptException{
       Iterator<String> value = initial.iterator();
       String prev = null;
       
       while (value.hasNext()) { 
            String c = value.next();
            if(isOperand(c)){
                result.add(c);
                LOG.info(c + " is added to reslut");
            }
            else if(c.equals("(")){
                if(prev != null && isOperand(prev)){
                    operators.push("*");
                }
                operators.push(c);
            }
            else if(c.equals(")")){
                while(!operators.isEmpty() && !operators.peek().equals("(")){
                    LOG.info(operators.peek().charAt(0) + " is added to reslt");
                    result.add(operators.pop());
                }
                operators.pop();
            }
            else if (isOperator(c)){
                while(!operators.isEmpty() && isEqualOrHigherPrecedence(operators.peek(), c)){
                        LOG.info(operators.peek().charAt(0) + " is added to reslt");
                        result.add(operators.peek());
                        operators.pop();
                }
                operators.push(c);
            }
            prev = c;
        }
        while(!operators.isEmpty()){
            LOG.info(operators.peek().charAt(0) + " is added to reslt");
            result.add(operators.peek());
            operators.pop();
        }
       LOG.info("result = " + result.toString());
   }
   
    /**
     * Evaluates and calculate the postfix expression
     * 
     * @return 
     * @throws javax.script.ScriptException
     */
   public int evaluatePostfixExpression() throws ScriptException{
       int answer = 0;
       Deque<Integer> postfixResult = new ArrayDeque<>();
       Iterator<String> value = result.iterator();
       
       while (value.hasNext()) { 
            String s = value.next();
            if(isOperand(s)){
                postfixResult.push(s.charAt(0) - '0');
            }
            else{
                Integer first = postfixResult.pop();
                Integer second = postfixResult.pop();
                if(s.equals("/") && first == 0){
                    throw new java.lang.ArithmeticException();
                }
                LOG.info(second + " " + s + " " + first);
                ScriptEngineManager manager = new ScriptEngineManager();
                ScriptEngine engine = manager.getEngineByName("js");
                answer = (Integer)engine.eval(second + s + first);
                postfixResult.addFirst(answer);
            } 
        }
       return answer;
   }
}
